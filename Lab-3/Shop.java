import java.util.Scanner;
public class Shop{
	public static void main(String[] args){
		Phone[] products = new Phone[4];
		for(int i=0;i<products.length;i++){
			products[i]=new Phone();
			System.out.println("Enter price");
			products[i].price=new Scanner(System.in).nextInt();
			System.out.println("Enter brand");
			products[i].brand=new Scanner(System.in).nextLine();
			System.out.println("Enter condition");
			products[i].condition=new Scanner(System.in).nextLine();
		}
		System.out.println(products[3].price +" "+ products[3].brand +" "+ products[3].condition);
		System.out.println("Initial price of the last product: "+ products[3].price);
		products[3].finalPrice();
		System.out.println("Final price of the last product after taking in consideration its condition: "+ products[3].price);
	}
}